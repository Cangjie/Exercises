# A + B Problem II
Time Limit: 2000/1000 MS (Java/Others)    Memory Limit: 65536/32768 K (Java/Others)

Total Submission(s): 589047    Accepted Submission(s): 112312


## Problem Description
I have a very simple problem for you. Given two integers A and B, your job is to calculate the Sum of A + B.
 

## Input
The first line of the input contains an integer T(1<=T<=20) which means the number of test cases. Then T lines follow, each line consists of two positive integers, A and B. Notice that the integers are very large, that means you should not process them by using 32-bit integer. You may assume the length of each integer will not exceed 1000.
 

## Output
For each test case, you should output two lines. The first line is "Case #:", # means the number of the test case. The second line is the an equation "A + B = Sum", Sum means the result of A + B. Note there are some spaces int the equation. Output a blank line between two test cases.
 

## Sample Input
2
1 2
112233445566778899 998877665544332211
 

## Sample Output
Case 1:
1 + 2 = 3

Case 2:
112233445566778899 + 998877665544332211 = 1111111111111111110
 

## Author
Ignatius.L

## 【CPP实现】
```cpp
// Created by yezeyu on 2022/11/16.
// HDU1002
// 超大整数加法运算
// 使用字符数组存储超大整数，再模拟加法计算

#include <iostream>
#include <cstring>

using namespace std;

char a[1005], b[1005], r[1010];

// 出现较多数据时，使用scanf和printf的效率会更高
int main() {
    int round;
    scanf("%d", &round);
    for(int kase = 1; kase <= round; kase++) {
        // 使用scanf将两个大整数存入两个字符数组
        // 先输入的会存在数组的低位，如数12345，a[0]=1,a[1]=2,...,a[4]=5
        scanf("%s%s", a, b);
        // 清空结果数组，重置成全0
        memset(r, 0, sizeof(r));
        // 定义变量存储数值真实长度
        int aLen, bLen, rLen;
        aLen = strlen(a);
        bLen = strlen(b);
        rLen = aLen;
        // 由于先输入的数字字符（数值的高位）会存在数组的低位，而计算需要进位，会增加数组长度，
        // 所以需要把其中一个大数的原数组“倒装”置入结果数组
        for(int i = aLen - 1; i >= 0; i--) {
            r[aLen - 1 - i ] = a[i] - '0'; // 数字字符减字符0可以获得真实整数数值
        }
        if(bLen > rLen) {
            rLen = bLen;
        }
        int carry = 0; // 记录进位，初始为0
        for(int i = bLen - 1; i >= 0; i--) {
            r[bLen - 1 - i] += b[i] - '0' + carry; // 需要加上进位
            carry = r[bLen - 1 - i] / 10; // 10进制，也可以使用变量
            r[bLen - 1 - i] %= 10;
        }
        // a与b长度不同，若有进位则继续计算
        for(int i = 0; carry > 0; i++) {
            r[bLen + i] += carry;
            carry = r[bLen + i] / 10;
            r[bLen + i] %= 10;
            // 判断是否需要更新结果数值长度
            if(bLen + i + 1 > rLen) {
                rLen = bLen + i + 1;
            }
        }
        // 去除前导0
        for(int i = rLen - 1; i >= 0; i--) {
            if(r[i]) {
                rLen = i + 1;
                break;
            }
        }

        // 按格式输出
        printf("Case %d:\n", kase);
        for(int i = 0; i < aLen; i++) {
            printf("%c", a[i]);
        }
        printf(" + ");
        for(int i = 0; i < bLen; i++) {
            printf("%c", b[i]);
        }
        printf(" = ");
        for(int i = rLen - 1; i >= 0; i--) {
            printf("%c", r[i] + '0');
        }
        printf("\n");
        if(kase != round) {
            printf("\n");
        }
    }
    return 0;
}
```