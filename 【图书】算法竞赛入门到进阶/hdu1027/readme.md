# Ignatius and the Princess II
Time Limit: 2000/1000 MS (Java/Others)    Memory Limit: 65536/32768 K (Java/Others)

Total Submission(s): 20753    Accepted Submission(s): 11850


## Problem Description
Now our hero finds the door to the BEelzebub feng5166. He opens the door and finds feng5166 is about to kill our pretty Princess. But now the BEelzebub has to beat our hero first. feng5166 says, "I have three question for you, if you can work them out, I will release the Princess, or you will be my dinner, too." Ignatius says confidently, "OK, at last, I will save the Princess."

"Now I will show you the first problem." feng5166 says, "Given a sequence of number 1 to N, we define that 1,2,3...N-1,N is the smallest sequence among all the sequence which can be composed with number 1 to N(each number can be and should be use only once in this problem). So it's easy to see the second smallest sequence is 1,2,3...N,N-1. Now I will give you two numbers, N and M. You should tell me the Mth smallest sequence which is composed with number 1 to N. It's easy, isn't is? Hahahahaha......"
Can you help Ignatius to solve this problem?
 

## Input
The input contains several test cases. Each test case consists of two numbers, N and M(1<=N<=1000, 1<=M<=10000). You may assume that there is always a sequence satisfied the BEelzebub's demand. The input is terminated by the end of file.
 

## Output
For each test case, you only have to output the sequence satisfied the BEelzebub's demand. When output a sequence, you should print a space between two numbers, but do not output any spaces after the last number.
 

## Sample Input
6 4
11 8
 

## Sample Output
1 2 3 5 6 4
1 2 3 4 5 6 7 9 8 11 10
 

## Author
Ignatius.L

## 【CPP实现】
```cpp
//
// Created by yezeyu on 2022/11/18.
// HDU1027

#include <iostream>
#include <algorithm>
#include <cstring>

#define maxN 1000

using namespace std;

int a[maxN];

int main() {
    int n, m;
    while (cin >> n >> m) {
        // memset(a, 0, sizeof(a)); // 不需要memset了，因为数组长度是清楚的
        for(int i = 1; i <= n; i++) {
            a[i] = i;
        }
        // 不需要排序了，因为赋值时已经按最小字典序排好了
        int b = 1;
        do {

        } while (b != m  && b++ && next_permutation(a+1, a+n+1));
            // next_permutation的意思是下一个排列，与其相对的是prev_permutation
            // 基本定义如下：
            // next_permutaion(起始地址，末尾地址+1)
            // next_permutaion(起始地址，末尾地址+1，自定义排序)
            // 可以使用默认的升序排序，也可以使用自定义的排序方法
        for(int i = 1; i < n; i++) {
            cout << a[i] << " "; // 最后一个元素后不需要空格
        }
        cout << a[n] << endl;
    }
    return 0;
}
// 补充参考：https://blog.csdn.net/love20165104027/article/details/79809291
```