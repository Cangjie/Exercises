# HDU1001
Sum Problem

## 题目地址
https://acm.hdu.edu.cn/showproblem.php?pid=1001

## 解题思路
这一题，当使用数学求和公式时，要考虑算n*(n+1)可能溢出的特殊情况

题目提示“You may assume the result will be in the range of 32 bit signed integer”

可以认为求和结果是32位有符号整数，虽然n*(n+1)/2一定是32位有符号整数范围内，但n*(n+1)却未必是

所以应该想办法让除法先做，然后再做乘法，避免溢出

做除法的时候还应注意整除的问题，整数相加肯定还是整数

所以需要先判断是否能被2整除，避免丢失精度

## 【CPP实现】
```cpp
//
// Created by yezeyu on 2022/11/16.
//
// 不要小看这一题，当使用数学求和公式时，要考虑算n*(n+1)可能溢出的特殊情况
// 题目提示“You may assume the result will be in the range of 32 bit signed integer”
// 可以认为求和结果是32位有符号整数，虽然n*(n+1)/2一定是32位有符号整数范围内，但n*(n+1)却未必是
// 所以应该想办法让除法先做，然后再做乘法，避免溢出
// 做除法的时候还应注意整除的问题，整数相加肯定还是整数
// 所以需要先判断是否能被2整除，避免丢失精度

#include <iostream>

using namespace std;

int main() {
    int n;
    while(~scanf("%d", &n) && n) {
        cout << (n%2 ? (n+1)/2*n : n/2*(n+1)) << endl << endl;
    }
    return 0;
}
```