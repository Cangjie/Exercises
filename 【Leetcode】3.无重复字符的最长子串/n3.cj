/**
3. 无重复字符的最长子串
https://leetcode.cn/problems/longest-substring-without-repeating-characters/description/

解题思路：
    滑动窗口，利用s中的每个byte作为右指针，每次查询window中是否已存在右指针指向的byte
    如果存在，滑动左指针，并且滑过的路径全部设为false，直到滑倒右指针为止
    如果不存在，右指针会自然递增，左指针不变，右-左+1 更新ans的值（如果比原来的ans大）
时间、空间复杂度：
    O(n) O(1)
仓颉语法：
    迭代器iterator()和enumerate()的基本使用，enumerate() 除了会包含T，还会包含迭代的次数（这里对应下标）
    扩展语法的基本使用，本例为Int64扩展了max方法
    类型转换，UInt8 转换为 Int64 语法为 ：Int64(x)
 */
package cangjieLeetcode.n1_n100
import std.collection.*
class SolutionN3 {
    func lengthOfLongestSubstring(s: String): Int64 {
        var left = 0
        var ans = 0
        var window = Array<Bool>(128,{b=>false})
        
        for ((right,c) in s.iterator().enumerate()) {
            let i = Int64(c)
            while(window[i]) {
                window[Int64(s[left])] = false
                left += 1
            }
            window[i] = true
            ans = ans.max(right - left + 1)
        }
        ans
    }
}
extend Int64 {
    public func max(other:Int64):Int64 {
        if (this > other){
            return this
        }
        return other
    }
}