
## 第一题，两数之和
 * 题目描述： https://leetcode.cn/problems/two-sum/
## 解题思路：
    使用HashMap存放信息，K：数字本身，V：数字下标
    遍历数组，每次用target - 当前数字 得到一个数，在map中查询是否有这个K
    如果查到：返回当前数字的下标和查到的下标
    否则：将当前数字作为K，当前下标作为V，放进map中

## 时间、空间复杂度：
    O(n) O(n)
    
## 仓颉语法：
* 区间值：0..x    
* HashMap的基本使用
* if-let 判断枚举语法
