// cjlint-ignore -start !G.NAM.02 !G.OTH.03 !G.FUN.01 !G.NAM.04
/**
 * @author unravel
 * @see https://gitcode.com/unravel/cjalgorithm/overview
 */

package cj_algorithm.swordoffer

import cj_algorithm.leetcode.ThinkMath
import usablemacros.verbose.dprintMultline

public class SwordOfferMath {
    let math = ThinkMath()
    /**
    给一个长度为 n 的数组，数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。
    例如输入一个长度为9的数组[1,2,3,2,2,2,5,4,2]。由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2。
     */
    // MARK: 数组中出现次数超过一半的数字
    // https://www.nowcoder.com/practice/e8a1b01a2df14cb2b228b30ee6a92163
    // 思想: 计数一个数最大的出现次数，相同加1不同减1，为0时就换数字
    func moreThanHalfNum_Solution(nums: Array<Int>): Int {
        math.majorityElement(nums)
    }

    /**
    社团共有 num 位成员参与破冰游戏，编号为 0 ~ num-1。成员们按照编号顺序围绕圆桌而坐。社长抽取一个数字 target，从 0 号成员起开始计数，排在第 target 位的成员离开圆桌，且成员离开后从下一个成员开始计数。请返回游戏结束时最后一位成员的编号。
     */
    // MARK: 圆圈中最后剩下的数
    // https://www.nowcoder.com/practice/f78a359491e64a50bce2d89cff857eb6
    // https://leetcode.cn/problems/yuan-quan-zhong-zui-hou-sheng-xia-de-shu-zi-lcof/
    // 思想: 约瑟夫环问题
    func lastRemaining(n: Int, m: Int): Int {
        // 特殊输入的处理，没有人的时候就找不到最后一个剩下的人
        if (n == 0) {
            return -1
        }

        // 递归返回条件，只有一个人的时候，说明返回的就是这个人，又由于人是从0开始增的，所以第一个人是0
        if (n == 1) {
            return 0
        }

        return (lastRemaining(n - 1, m) + m) % n
    }

    func lastRemaining2(n: Int, m: Int): Int {
        // 特殊输入的处理，没有人的时候就找不到最后一个剩下的人
        if (n == 0) {
            // 一个没有人时的默认返回值，可以是-1，也可以是-999或任何约定好的数字
            return -1
        }

        // 递归返回条件，只有一个人的时候，说明返回的就是这个人，又由于人是从0开始增的，所以第一个人是0
        if (n == 1) {
            return 0
        }

        var last = 0
        // 最后一轮剩下2个人，所以从2开始反推
        for (i in 2..=n) {
            last = (last + m) % i
        }
        return last
    }

    /**
    输入一个整数 n ，求 1～n 这 n 个整数的十进制表示中 1 出现的次数
    例如， 1~13 中包含 1 的数字有 1 、 10 、 11 、 12 、 13 因此共出现 6 次
     */
    // MARK: 从 1 到 n 整数中 1 出现的次数
    // https://www.nowcoder.com/practice/bd7f978302044eee894445e244c7eee6
    // 思想: 按照个位、十位、百位上的1来计算出现的1的个数
    func numberOf1Between1AndN_Solution(n: Int): Int {
        var cnt = 0
        var digit = 1
        while (digit <= n) {
            let high = n / digit
            let reminder = n % digit
            cnt += (high + 8) / 10 * digit + if (high % 10 == 1) {
                reminder + 1
            } else {
                0
            }

            digit *= 10
        }
        return cnt
    }

    public func verify() {
        @dprintMultline(
            // 3
            moreThanHalfNum_Solution([3, 3, 3, 3, 2, 2, 2])
            // 2
            lastRemaining(10, 17)
            // 1
            lastRemaining2(7, 4)
            // 6
            numberOf1Between1AndN_Solution(13)
        )
    }
}
// cjlint-ignore -end
