// cjlint-ignore -start !G.NAM.02 !G.OTH.03 !G.FUN.01
/**
 * @author unravel
 * @see https://gitcode.com/unravel/cjalgorithm/overview
 */

package cj_algorithm.swordoffer

import std.collection.ArrayList
import usablemacros.verbose.dprintMultline

public class SwordOfferArrayMatrix {
    /**
    设备中存有 n 个文件，文件 id 记于数组 documents。若文件 id 相同，则定义为该文件存在副本。请返回任一存在副本的文件 id。
     */
    // MARK: 数组中重复的数字
    // https://leetcode.cn/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof/description/
    // 思想: 将每个数字放到它对应的位置上，比如nums[i] 放到 i的位置上，如果有两个放到了一个地方，就可以校验出来
    // 另外一个通用的思想，把遍历过的数字放到集合里，若集合里包含此数据，说明有重复的
    func findRepeatNumber(nums: Array<Int>): Int {
        // 赋值新数组，因为内部会进行交换
        let compareNums = nums

        for (i in 0..compareNums.size) {
            while (compareNums[i] != i) {
                // 得出它该放的位置
                let j = compareNums[i]
                // 如果它本来的位置上已经有数据，就重复了
                if (j == compareNums[j]) {
                    return j
                }
                // 交换i 和  nums[i] 位置处的数字
                compareNums.swap(i, j)
            }
        }
        return -1
    }

    /**
    m*n 的二维数组 plants 记录了园林景观的植物排布情况，具有以下特性：

    每行中，每棵植物的右侧相邻植物不矮于该植物；
    每列中，每棵植物的下侧相邻植物不矮于该植物。

    请判断 plants 中是否存在目标高度值 target。
     */
    // MARK: 二维数组中的查找
    // https://leetcode.cn/problems/er-wei-shu-zu-zhong-de-cha-zhao-lcof/
    // 思想: 找规律，从左下角或右上角入手
    func findNumberIn2DArray(matrix: Array<Array<Int>>, target: Int): Bool {
        if (matrix.isEmpty() || matrix[0].isEmpty()) {
            return false
        }
        let rows = matrix.size
        let cols = matrix[0].size
        // 从右上角开始
        var row = 0
        var col = cols - 1
        while (row < rows && col >= 0) {
            // 获取row行col列的数据
            let valueAtRowCol = matrix[row][col]
            if (target == valueAtRowCol) {
                return true
            } else if (target > valueAtRowCol) {
                row += 1
            } else {
                col -= 1
            }
        }
        return false
    }

    /**
    请实现一个函数，将一个字符串s中的每个空格替换成“%20”。
    例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy。
     */
    // MARK: 替换空格
    // https://www.nowcoder.com/practice/0e26e5551f2b489b9f58bc83aa4b6c68
    // 思想: 给str扩容，之后双指针分别从最后往前移动。这样可以防止插入%20造成的内存移动操作
    func replaceSpace(s: String): String {
        return s.replace(" ", "%20")
    }

    /**
    给定一个二维数组 array，请返回「螺旋遍历」该数组的结果。

    螺旋遍历：从左上角开始，按照 向右、向下、向左、向上 的顺序 依次 提取元素，然后再进入内部一层重复相同的步骤，直到提取完所有元素。
     */
    // MARK: 顺时针打印矩阵
    // https://leetcode.cn/problems/shun-shi-zhen-da-yin-ju-zhen-lcof/
    // 思想: 采用四个边界法，左右->上下->右左->下上 如此循环，期间修改边界
    public func spiralOrder(matrix: Array<Array<Int>>): Array<Int> {
        let ret = ArrayList<Int>()
        if (matrix.isEmpty() || matrix[0].isEmpty()) {
            return ret.toArray()
        }

        // 左边界， 右边界，上边界，下边界
        var left = 0
        var right = matrix[0].size - 1
        var up = 0
        var down = matrix.size - 1
        // 这里使用while true会让逻辑理解起来更简单
        while (true) {
            // 最上面一行
            for (col in left..=right) {
                ret.append(matrix[up][col])
            }
            // 向下逼近
            up += 1
            // 判断是否越界
            if (up > down) {
                break
            }

            // 最右边一行
            for (row in up..=down) {
                ret.append(matrix[row][right])
            }
            // 向左逼近
            right -= 1
            // 判断是否越界
            if (left > right) {
                break
            }

            // 最下面一行
            for (col in right..=left : -1) {
                ret.append(matrix[down][col])
            }
            // 向上逼近
            down -= 1
            // 判断是否越界
            if (up > down) {
                break
            }

            // 最左边一行
            for (row in down..=up : -1) {
                ret.append(matrix[row][left])
            }
            // 向右逼近
            left += 1
            // 判断是否越界
            if (left > right) {
                break
            }
        }
        return ret.toArray()
    }

    /**
    某套连招动作记作仅由小写字母组成的序列 arr，其中 arr[i] 第 i 个招式的名字。请返回第一个只出现一次的招式名称，如不存在请返回空格。
     */
    // MARK: 第一个只出现一次的字符位置
    // https://leetcode.cn/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof/description/
    func firstUniqChar(s: String): Rune {
        let sChars = s.toRuneArray()
        // 总共26个字母，数组26个长度就可以
        let cntArray = Array<Int>(26, repeat: 0)
        // 下标计算的基准
        let aAsciiValue = UInt32(r"a")
        // 第一遍统计次数
        for (ch in sChars) {
            let ind = Int64(UInt32(ch) - aAsciiValue)
            cntArray[ind] += 1
        }

        // 第二遍判断第一个次数为1的ch
        for (ch in sChars) {
            let ind = Int64(UInt32(ch) - aAsciiValue)
            if (cntArray[ind] == 1) {
                return ch
            }
        }

        return r" "
    }

    public func verify() {
        @dprintMultline(
            // 0 或 5
            findRepeatNumber([2, 5, 3, 0, 5, 0])
            // true
            findNumberIn2DArray([[2,3,6,8],[4,5,8,9],[5,9,10,12]],8)
            // "We%20Are%20Happy"
            replaceSpace("We Are Happy")
            // [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
            spiralOrder([[1,2,3,4],[12,13,14,5],[11,16,15,6],[10,9,8,7]])
            // a
            firstUniqChar("abbccdeff")
        )
    }
}
// cjlint-ignore -end
