// cjlint-ignore -start !G.NAM.02 !G.OTH.03 !G.FUN.01
/**
 * @author unravel
 * @see https://gitcode.com/unravel/cjalgorithm/overview
 */

package cj_algorithm.swordoffer

import usablemacros.verbose.dprintMultline

public class SwordOfferBitOperation {
    /**
    编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为 汉明重量).）。
     */
    // MARK: 二进制中 1 的个数
    // https://leetcode.cn/problems/er-jin-zhi-zhong-1de-ge-shu-lcof/description/
    // 思想: n & (n - 1) 会将二进制中最右侧的1变为0
    func numberOf1In(n: Int): Int {
        var cnt = 0
        var mN = n
        while (mN != 0) {
            mN &= (mN - 1)
            cnt += 1
        }
        return cnt
    }

    /**
    给你一个整数数组 nums，其中恰好有两个元素只出现一次，其余所有元素均出现两次。 找出只出现一次的那两个元素。你可以按 任意顺序 返回答案。
     */
    // MARK: 数组中只出现一次的数字
    // https://leetcode.cn/problems/single-number-iii/description/
    func findNumsAppearOnce(nums: Array<Int>): Array<Int> {
        var bitmask = 0
        // 异或到最后，就相当于是那两个数字的异或
        for (num in nums) {
            bitmask ^= num
        }

        // x & -x 会将得到x中最右侧为1的位
        let diff = bitmask & -bitmask

        let res = [0, 0]

        for (num in nums) {
            if ((num & diff) == 0) {
                res[0] ^= num
            } else {
                res[1] ^= num
            }
        }
        return res
    }
    public func verify() {
        @dprintMultline(
            // 31
            numberOf1In(4294967293)
            // [3, 5] 或 [5, 3]
            findNumsAppearOnce([1,2,1,3,2,5])
        )
    }
}
// cjlint-ignore -end
