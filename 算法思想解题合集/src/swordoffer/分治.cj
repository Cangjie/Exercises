// cjlint-ignore -start !G.NAM.02 !G.OTH.03 !G.FUN.01
/**
 * @author unravel
 * @see https://gitcode.com/unravel/cjalgorithm/overview
 */

package cj_algorithm.swordoffer

import usablemacros.verbose.dprintMultline

public class SwordOfferDivid {
    /**
    描述
    实现函数 double Power(double base, int exponent)，求base的exponent次方。

    注意：
    1.保证base和exponent不同时为0。
    2.不得使用库函数，同时不需要考虑大数问题
    3.有特殊判题，不用考虑小数点后面0的位数。
     */
    // MARK: 数值的整数次方
    // https://www.nowcoder.com/practice/1a834e5e3e1a4b7ba251417554e07c00
    // 思想:
    // exponent 为奇数时 base^exponent = (base * base)^(exponent/2) * base
    // exponent 为偶数时 base^expoent = (base * base)^(exponent/2)
    // 当exponent 为负数时，最终的结果为正数时的倒数
    func cusPower(base: Float64, exponent: Int): Float64 {
        var isNegative = false
        var mExponent = exponent

        if (mExponent < 0) {
            mExponent = -mExponent
            isNegative = true
        }

        // 计算幂数
        func pow(x: Float64, n: Int): Float64 {
            // 任何数的0次幂都是1
            if (n == 0) {
                return 1.0
            }
            // 任何数的1次幂是其本身
            if (n == 1) {
                return x
            }
            // 折半计算
            var res = pow(x, n / 2)
            res = res * res
            if (n % 2 != 0) {
                res *= x
            }
            return res
        }

        let res = pow(base, mExponent)
        return if (isNegative) {
            1.0 / res
        } else {
            res
        }
    }

    public func verify() {
        @dprintMultline(
            // 9.26100
            cusPower(2.10000,3)
        )
    }
}
// cjlint-ignore -end
