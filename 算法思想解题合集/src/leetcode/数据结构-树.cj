// cjlint-ignore -start !G.OTH.03 !G.NAM.02 !G.NAM.03 !G.FUN.01
/**
 * @author unravel
 * @see https://gitcode.com/unravel/cj_algorithm/overview
 */

package cj_algorithm.leetcode

import std.math.abs
import std.collection.{HashMap, ArrayList}
import cj_algorithm.structures.{ListNode, TreeNode, Stack, Queue, KVPair, null}
import usablemacros.verbose.dprintMultline

public class DataStructureTree {
    /**
    给定一个二叉树 root ，返回其最大深度。
    二叉树的 最大深度 是指从根节点到最远叶子节点的最长路径上的节点数
     */
    // MARK: 树的高度
    // 剑指 Offer 55 - I. 二叉树的深度
    // https://leetcode-cn.com/problems/er-cha-shu-de-shen-du-lcof/
    // 104. 二叉树的最大深度
    // https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/description/
    // MARK: 二叉树的深度
    // 思想：最长的路径
    public func maxDepth(root: ?TreeNode): Int {
        if (let Some(rootNode) <- root) {
            // 本层 加 左右子层的最大深度
            let left = maxDepth(rootNode.left)
            let right = maxDepth(rootNode.right)
            return 1 + max(left, right)
        } else {
            return 0
        }
    }

    func maxDepth1(root: ?TreeNode): Int {
        if (root.isNone()) {
            return 0
        }
        var queue = Queue<TreeNode>()
        queue.enqueue(root.getOrThrow())
        var res = 0
        while (!queue.isEmpty()) {
            let size = queue.size
            for (_ in 0..size) {
                let node = queue.dequeue().getOrThrow()
                if (node.left.isSome()) {
                    queue.enqueue(node.left.getOrThrow())
                }
                if (node.right.isSome()) {
                    queue.enqueue(node.right.getOrThrow())
                }
            }
            res += 1
        }
        return res
    }

    /**
    给定一个二叉树，判断它是否是 平衡二叉树

     */
    // MARK: 判断一棵树是否是平衡树
    // https://leetcode-cn.com/problems/balanced-binary-tree/description/
    // MARK: 平衡二叉树
    // 输入一棵二叉树，判断该二叉树是否是平衡二叉树
    // 思想：  平衡二叉树左右子树高度差不超过 1
    public func isBalanced(root: ?TreeNode): Bool {
        var balanced = true

        // 在计算深度的过程中，设置是否平衡的标识
        func maxDepth(rt: ?TreeNode): Int {
            if (rt.isNone() || !balanced) {
                return 0
            }
            let rtNode = rt.getOrThrow()
            let l = maxDepth(rtNode.left)
            let r = maxDepth(rtNode.right)
            // 子树高度差大于1，则不是平衡二叉树
            if (abs(l - r) > 1) {
                balanced = false
            }
            return max(l, r) + 1
        }

        maxDepth(root)
        return balanced
    }

    /**
    给你一棵二叉树的根节点，返回该树的 直径 。
    二叉树的 直径 是指树中任意两个节点之间最长路径的 长度 。这条路径可能经过也可能不经过根节点 root 。
    两节点之间路径的 长度 由它们之间边数表示
     */
    // MARK: 树中两节点之间的最长路径
    // https://leetcode-cn.com/problems/diameter-of-binary-tree/description/
    func diameterOfBinaryTree(root: ?TreeNode): Int {
        var maxDepth = 0
        // 在计算深度的过程中，计算左右子树高度和的最大值

        func depth(rt: ?TreeNode): Int {
            if (rt.isNone()) {
                return 0
            }
            let rtNode = rt.getOrThrow()
            let leftDepth = depth(rtNode.left)
            let rightDepth = depth(rtNode.right)
            // 保存最深层数
            maxDepth = max(maxDepth, leftDepth + rightDepth)
            return max(leftDepth, rightDepth) + 1
        }
        depth(root)
        return maxDepth
    }

    /**
    给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点
     */
    // MARK: 翻转二叉树
    // https://leetcode-cn.com/problems/invert-binary-tree/description/
    // MARK: 二叉树的镜像
    // https://www.nowcoder.com/practice/a9d0ecbacef9410ca97463e4a5c83be7
    // 思想: 采用递归翻转二叉树。单子是一个二层树的翻转
    func invertTree(root: ?TreeNode): ?TreeNode {
        if (root.isNone()) {
            return root
        }
        // 后面的操作会改变 left的指针，所以先保存下来
        let rtNode = root.getOrThrow()
        let left = rtNode.left
        rtNode.left = invertTree(rtNode.right)
        rtNode.right = invertTree(left)
        return root
    }

    /**
    给你两棵二叉树： root1 和 root2 。
    想象一下，当你将其中一棵覆盖到另一棵之上时，两棵树上的一些节点将会重叠（而另一些不会）。你需要将这两棵树合并成一棵新二叉树。合并的规则是：如果两个节点重叠，那么将这两个节点的值相加作为合并后节点的新值；否则，不为 null 的节点将直接作为新二叉树的节点。
    返回合并后的二叉树。
    注意: 合并过程必须从两个树的根节点开始
     */
    // MARK: 归并两棵树
    // https://leetcode-cn.com/problems/merge-two-binary-trees/description/
    func mergeTrees(
        root1: ?TreeNode,
        root2: ?TreeNode
    ): ?TreeNode {
        if (root1.isNone() || root2.isNone()) {
            // 如果root1和root2，其中有一颗为空，则返回不为空的那棵树
            return if (root1.isNone()) {
                root2
            } else {
                root1
            }
        }
        let t1 = root1.getOrThrow()
        let t2 = root2.getOrThrow()

        // 两颗都不为空，则构建 节点和左右子树
        let root = TreeNode(t1.val + t2.val)
        root.left = mergeTrees(t1.left, t2.left)
        root.right = mergeTrees(t1.right, t2.right)
        return root
    }

    /**
    给你二叉树的根节点 root 和一个表示目标和的整数 targetSum 。判断该树中是否存在 根节点到叶子节点 的路径，这条路径上所有节点值相加等于目标和 targetSum 。如果存在，返回 true ；否则，返回 false 。
    叶子节点 是指没有子节点的节点
     */
    // MARK: 判断根节点到叶子节点的路径和是否等于一个数
    // https://leetcode-cn.com/problems/path-sum/description/
    func hasPathSum(
        root: ?TreeNode,
        targetSum: Int
    ): Bool {
        if (root.isNone()) {
            return false
        }
        let rootNode = root.getOrThrow()
        // 判断是否是叶子节点，并且和判断的值相等
        if (rootNode.left.isNone() && rootNode.right.isNone() && rootNode.val == targetSum) {
            return true
        }
        // 判断余下的值是否有相等的节点
        let remind = targetSum - rootNode.val
        // 判断左右子树
        return hasPathSum(rootNode.left, remind) || hasPathSum(rootNode.right, remind)
    }

    /**
    给定一个二叉树的根节点 root ，和一个整数 targetSum ，求该二叉树里节点值之和等于 targetSum 的 路径 的数目。
    路径 不需要从根节点开始，也不需要在叶子节点结束，但是路径方向必须是向下的（只能从父节点到子节点）
     */
    // MARK: 统计路径和等于一个数的路径 有多少个
    // https://leetcode-cn.com/problems/path-sum-iii/description/
    func pathSum(root: ?TreeNode, targetSum: Int): Int {
        if (root.isNone()) {
            return 0
        }
        // 判断一个节点，以及它所挂载的子树是否为和
        func pathSumStartWithRoot(
            rt: ?TreeNode,
            sum: Int
        ): Int {
            if (rt.isNone()) {
                return 0
            }
            var ret = 0
            let rtNode = rt.getOrThrow()
            // 节点自身和值相等记1
            if (rtNode.val == sum) {
                ret += 1
            }
            // 剩余值
            let remind = sum - rtNode.val
            // 统计这个节点下的左右子树
            ret += pathSumStartWithRoot(rtNode.left, remind) + pathSumStartWithRoot(rtNode.right, remind)
            return ret
        }
        let rootNode = root.getOrThrow()
        // 统计根节点和左右子树
        return pathSumStartWithRoot(rootNode, targetSum) + pathSum(rootNode.left, targetSum) + pathSum(rootNode.right,
            targetSum)
    }

    /**
    给你两棵二叉树 root 和 subRoot 。检验 root 中是否包含和 subRoot 具有相同结构和节点值的子树。如果存在，返回 true ；否则，返回 false 。
    二叉树 tree 的一棵子树包括 tree 的某个节点和这个节点的所有后代节点。tree 也可以看做它自身的一棵子树
     */
    // MARK: 判断一棵树是否是另一棵树的子树
    // https://leetcode-cn.com/problems/subtree-of-another-tree/description/
    // MARK: 树的子结构
    // https://www.nowcoder.com/practice/6e196c44c7004d15b1610b9afca8bd88
    // 思想: 递归方法。单子是一个只有两个节点的2层树是否相等
    // https://leetcode.cn/problems/shu-de-zi-jie-gou-lcof/
    func isSubtree(
        root: ?TreeNode,
        subRoot: ?TreeNode
    ): Bool {
        if (root.isNone()) {
            return false
        }
        // 判断是不是某个节点的子树
        func isSubtreeWithRoot(s: ?TreeNode, t: ?TreeNode): Bool {
            match ((s, t)) {
                // 都不为空
                case (Some(sNode), Some(vNode)) =>
                    if (sNode.val == vNode.val) {
                        // 递归判断左右子树
                        return isSubtreeWithRoot(sNode.left, vNode.left) && isSubtreeWithRoot(sNode.right, vNode.right)
                    } else {
                        false
                    }
                // 都为空
                case (None, None) => true
                // 其中一个为空
                case _ => false
            }
        }
        let rootNode = root.getOrThrow()
        // 判断是否是本树或者左右子树的子树
        return isSubtreeWithRoot(rootNode, subRoot) || isSubtree(rootNode.left, subRoot) || isSubtree(rootNode.right,
            subRoot)
    }

    /**
    给你一个二叉树的根节点 root ， 检查它是否轴对称
     */
    // MARK: 判断一棵树是否对称
    // MARK: 对称的二叉树
    // https://leetcode-cn.com/problems/symmetric-tree/description/
    func isSymmetric(root: ?TreeNode): Bool {
        if (root.isNone()) {
            return true
        }
        // 判断两棵树是否互为镜像树
        func isSymmetric(t1: ?TreeNode, t2: ?TreeNode): Bool {
            match ((t1, t2)) {
                // 都 不为空
                case (Some(t1Node), Some(t2Node)) =>
                    if (t1Node.val != t2Node.val) {
                        false
                    } else {
                        // 判断对应的子树是否是镜像树
                        return isSymmetric(t1Node.left, t2Node.right) && isSymmetric(t1Node.right, t2Node.left)
                    }
                // 都 为空
                case (None, None) => true
                // 有一个 为空
                case _ => false
            }
        }
        let rootNode = root.getOrThrow()
        // 判断根节点的左右子树是互为镜像树
        return isSymmetric(rootNode.left, rootNode.right)
    }

    /**
    给定一个二叉树，找出其最小深度。
    最小深度是从根节点到最近叶子节点的最短路径上的节点数量。
    说明：叶子节点是指没有子节点的节点
     */
    // MARK: 一棵树的最小深度
    // https://leetcode-cn.com/problems/minimum-depth-of-binary-tree/description/
    func minDepth(root: ?TreeNode): Int {
        if (root.isNone()) {
            return 0
        }
        let rootNode = root.getOrThrow()
        // 计算左右子树的深度
        let left = minDepth(rootNode.left)
        let right = minDepth(rootNode.right)
        // 判断左右子树是否已经是
        if (left == 0 || right == 0) {
            // 说明已经到了最靠上的一个叶子节点
            return left + right + 1
        }
        return min(left, right) + 1
    }

    /**
    给定二叉树的根节点 root ，返回所有左叶子之和
     */
    // MARK: 统计左叶子节点的和
    // https://leetcode-cn.com/problems/sum-of-left-leaves/description/
    func sumOfLeftLeaves(root: ?TreeNode): Int {
        if (root.isNone()) {
            return 0
        }
        let rootNode = root.getOrThrow()
        // 判断左节点是否为叶子节点
        let left = rootNode.left
        if (let Some(leftNode) <- left) {
            if (leftNode.left.isNone() && leftNode.right.isNone()) {
                return leftNode.val + sumOfLeftLeaves(rootNode.right)
            }
        }

        // 计算左右子树中左叶子节点
        return sumOfLeftLeaves(left) + sumOfLeftLeaves(rootNode.right)
    }

    /**
    给定一个二叉树的 root ，返回 最长的路径的长度 ，这个路径中的 每个节点具有相同值 。 这条路径可以经过也可以不经过根节点。
    两个节点之间的路径长度 由它们之间的边数表示
     */
    // MARK: 相同节点值的最大路径长度
    // https://leetcode-cn.com/problems/longest-univalue-path/
    func longestUnivaluePath(root: ?TreeNode): Int {
        var path = 0
        // 递归判断一棵树中 相邻的节点且值相同的 数量
        func dfs(rt: ?TreeNode): Int {
            if (rt.isNone()) {
                return 0
            }
            let rtNode = rt.getOrThrow()
            let left = dfs(rtNode.left)
            let right = dfs(rtNode.right)
            // 本节点和左节点相同
            let leftPath = if (rtNode.left.isSome() && rtNode.left.getOrThrow().val == rtNode.val) {
                left + 1
            } else {
                0
            }
            // 本节点和右节点相同
            let rightPath = if (rtNode.right.isSome() && rtNode.right.getOrThrow().val == rtNode.val) {
                right + 1
            } else {
                0
            }
            path = max(path, leftPath + rightPath)
            return max(leftPath, rightPath)
        }
        dfs(root)
        return path
    }

    /**
    小偷又发现了一个新的可行窃的地区。这个地区只有一个入口，我们称之为 root 。
    除了 root 之外，每栋房子有且只有一个“父“房子与之相连。一番侦察之后，聪明的小偷意识到“这个地方的所有房屋的排列类似于一棵二叉树”。 如果 两个直接相连的房子在同一天晚上被打劫 ，房屋将自动报警。
    给定二叉树的 root 。返回 在不触动警报的情况下 ，小偷能够盗取的最高金额
     */
    // MARK: 间隔遍历
    // https://leetcode-cn.com/problems/house-robber-iii/description/
    func rob(root: ?TreeNode): Int {
        let cache = HashMap<TreeNode, Int>()
        func robIn(rt: ?TreeNode): Int {
            if (rt.isNone()) {
                return 0
            }
            let rtNode = rt.getOrThrow()
            if (cache.contains(rtNode)) {
                return cache[rtNode]
            }
            var val1 = rtNode.val
            if (let Some(lNode) <- rtNode.left) {
                val1 += robIn(lNode.left) + robIn(lNode.right)
            }

            if (let Some(rNode) <- rtNode.right) {
                val1 += robIn(rNode.left) + robIn(rNode.right)
            }
            let val2 = robIn(rtNode.left) + robIn(rtNode.right)
            let ret = max(val1, val2)
            cache[rtNode] = ret
            return ret
        }
        return robIn(root)
    }

    func rob2(root: ?TreeNode): Int {
        /*
        每个节点可选择偷或者不偷两种状态，根据题目意思，相连节点不能一起偷

             当前节点选择偷时，那么两个孩子节点就不能选择偷了
             当前节点选择不偷时，两个孩子节点只需要拿最多的钱出来就行(两个孩子节点偷不偷没关系)

             我们使用一个大小为 2 的数组来表示 int[] res = new int[2] 0 代表不偷，1 代表偷
             任何一个节点能偷到的最大钱的状态可以定义为

             当前节点选择不偷：当前节点能偷到的最大钱数 = 左孩子能偷到的钱 + 右孩子能偷到的钱
             当前节点选择偷：当前节点能偷到的最大钱数 = 左孩子选择自己不偷时能得到的钱 + 右孩子选择不偷时能得到的钱 + 当前节点的钱数
         */
        func robInternal(rt: ?TreeNode): (Int, Int) {
            if (rt.isNone()) {
                return (0, 0)
            }
            let rtNode = rt.getOrThrow()
            let left = robInternal(rtNode.left)
            let right = robInternal(rtNode.right)

            // 当前节点选择不偷：当前节点能偷到的最大钱数 = 两个孩子节点只需要拿最多的钱出来就行(两个孩子节点偷不偷没关系)
            let ret0 = max(left[0], left[1]) + max(right[0], right[1])
            // 当前节点选择偷：当前节点能偷到的最大钱数 = 左孩子选择自己不偷时能得到的钱 + 右孩子选择不偷时能得到的钱 + 当前节点的钱数
            let ret1 = left[0] + right[0] + rtNode.val

            return (ret0, ret1)
        }

        let result = robInternal(root);
        return max(result[0], result[1])
    }

    /**
    给定一个非空特殊的二叉树，每个节点都是正数，并且每个节点的子节点数量只能为 2 或 0。如果一个节点有两个子节点的话，那么该节点的值等于两个子节点中较小的一个。

    更正式地说，即 root.val = min(root.left.val, root.right.val) 总成立。

    给出这样的一个二叉树，你需要输出所有节点中的 第二小的值 。

    如果第二小的值不存在的话，输出 -1
     */
    // MARK: 找出二叉树中第二小的节点
    // 二叉树根节点的值即为所有节点中的最小值，对于二叉树中的任意节点 xx，xx 的值不大于以 xx 为根的子树中所有节点的值
    // https://leetcode-cn.com/problems/second-minimum-node-in-a-binary-tree/description/
    func findSecondMinimumValue(root: ?TreeNode): Int {
        // 节点自身为空
        if (root.isNone()) {
            return -1
        }
        let rootNode = root.getOrThrow()
        // 节点左右孩子为空，即该节点为叶子节点
        if (rootNode.left.isNone() && rootNode.right.isNone()) {
            return -1
        }

        let rootVal = rootNode.val
        var leftVal = rootNode.left.getOrThrow().val
        var rightVal = rootNode.right.getOrThrow().val
        if (leftVal == rootVal) {
            // 如果左子节点的值和根节点相同，说明第二小的在左子树
            leftVal = findSecondMinimumValue(rootNode.left)
        }
        if (rightVal == rootVal) {
            // 如果右子节点的值和根节点相同，说明第二小的在右子树
            rightVal = findSecondMinimumValue(rootNode.right)
        }
        // 如果不都为-1，说明找到了第二小的值
        if (leftVal != -1 && rightVal != -1) {
            return min(leftVal, rightVal)
        }
        if (leftVal != -1) {
            return leftVal
        }
        return rightVal
    }

    /**
    给定一个非空二叉树的根节点 root , 以数组的形式返回每一层节点的平均值。与实际答案相差 10-5 以内的答案可以被接受
     */
    // MARK: 一棵树每层节点的平均数
    // https://leetcode-cn.com/problems/average-of-levels-in-binary-tree/submissions/
    func averageOfLevels(root: ?TreeNode): Array<Float64> {
        let ret = ArrayList<Float64>()
        if (root.isNone()) {
            return ret.toArray()
        }
        let queue = Queue<TreeNode>()
        queue.enqueue(root.getOrThrow())
        while (!queue.isEmpty()) {
            let cnt = queue.size
            var sum = 0
            for (_ in 0..cnt) {
                let node = queue.dequeue().getOrThrow()
                sum += node.val
                if (let Some(lNode) <- node.left) {
                    queue.enqueue(lNode)
                }
                if (let Some(rNode) <- node.right) {
                    queue.enqueue(rNode)
                }
            }
            ret.append(Float64(sum) / Float64(cnt))
        }
        return ret.toArray()
    }

    /**
    给定一个二叉树的 根节点 root，请找出该二叉树的 最底层 最左边 节点的值。

    假设二叉树中至少有一个节点
     */
    // MARK: 得到左下角的节点
    // https://leetcode-cn.com/problems/find-bottom-left-tree-value/description/
    func findBottomLeftValue(root: ?TreeNode): Int {
        if (root.isNone()) {
            return -1
        }
        let queue = Queue<TreeNode>()
        var node: TreeNode = root.getOrThrow()
        queue.enqueue(node)
        // 先右后左，保证node最后能指向最后一个左节点
        while (!queue.isEmpty()) {
            node = queue.dequeue().getOrThrow()
            if (let Some(rNode) <- node.right) {
                queue.enqueue(rNode)
            }
            if (let Some(lNode) <- node.left) {
                queue.enqueue(lNode)
            }
        }
        return node.val
    }

    // MARK: dfs深度优选搜索，二叉树的前中后序遍历
    func dfs(root: ?TreeNode) {
        // 前序遍历
        let ret = ArrayList<Int>()
        func preorder(rt: ?TreeNode): Unit {
            if (let Some(rtNode) <- rt) {
                ret.append(rtNode.val)
                preorder(rtNode.left)
                preorder(rtNode.right)
            }
        }
        // 中序遍历
        func inorder(rt: ?TreeNode): Unit {
            if (let Some(rtNode) <- rt) {
                preorder(rtNode.left)
                ret.append(rtNode.val)
                preorder(rtNode.right)
            }
        }
        // 后序遍历
        func postorder(rt: ?TreeNode): Unit {
            if (let Some(rtNode) <- rt) {
                preorder(rtNode.left)
                preorder(rtNode.right)
                ret.append(rtNode.val)
            }
        }

        println('${root}前序遍历结果${preorder(root)}')
    }

    /**
    给你二叉树的根节点 root ，返回它节点值的 前序 遍历
     */
    // MARK: 非递归实现二叉树的前序遍历
    // https://leetcode-cn.com/problems/binary-tree-preorder-traversal/description/
    func preorderTraversal(root: ?TreeNode): Array<Int> {
        let ret = ArrayList<Int>()

        let stack = Stack<?TreeNode>()
        stack.push(root)
        // 开始蹦迪
        while (!stack.isEmpty()) {
            // 取栈顶节点
            let node = stack.pop().getOrThrow()
            if (let Some(nNode) <- node) {
                ret.append(nNode.val)
                stack.push(nNode.right)
                stack.push(nNode.left)
            }
        }
        return ret.toArray()
    }

    /**
    给你一棵二叉树的根节点 root ，返回其节点值的 后序遍历
     */
    // MARK: 非递归实现二叉树的后序遍历
    // 和前序遍历入栈的顺序相反即可
    // https://leetcode-cn.com/problems/binary-tree-postorder-traversal/description/
    func postorderTraversal(root: ?TreeNode): Array<Int> {
        let ret = ArrayList<Int>()

        let stack = Stack<?TreeNode>()
        stack.push(root)
        // 开始蹦迪，最后反转ret
        while (!stack.isEmpty()) {
            let node = stack.pop().getOrThrow()
            if (let Some(nNode) <- node) {
                ret.append(nNode.val)
                stack.push(nNode.left)
                stack.push(nNode.right)
            }
        }
        // 取值要反过来
        ret.reverse()
        return ret.toArray()
    }

    /**
    给定一个二叉树的根节点 root ，返回 它的 中序 遍历 。
     */
    // MARK: 非递归实现二叉树的中序遍历
    // https://leetcode-cn.com/problems/binary-tree-inorder-traversal/description/
    func inorderTraversal(root: ?TreeNode): Array<Int> {
        let ret = ArrayList<Int>()
        if (root.isNone()) {
            return ret.toArray()
        }

        let stack = Stack<?TreeNode>()
        var cur = root

        while (cur.isSome() || !stack.isEmpty()) {
            // 左子树的左节点不断的进栈
            while (let Some(curNode) <- cur) {
                stack.push(curNode)
                cur = curNode.left
            }

            let node = stack.pop().getOrThrow().getOrThrow()
            ret.append(node.val)
            cur = node.right
        }
        return ret.toArray()
    }

    // MARK: 非递归实现二叉树的前中后序遍历
    func visibTreeByColor(root: ?TreeNode): Array<Int> {
        // 颜色标记法
        /*
             其核心方法如下：
             标记节点的状态，已访问的节点标记为 1，未访问的节点标记为 0
             遇到未访问的节点，将节点标记为 0，然后根据三序排序的要求，按照特定的顺序入栈

             // 前序 中→左→右 按照 右→左→中
             // 中序 左→中→右 按照 右→中→左
             // 后序 左→右→中 按照 中→右→左

             结果数组中加入标记为 1 的节点的值
         */
        let traversals = ArrayList<Int>()

        let stack = Stack<KVPair<Int, ?TreeNode>>([KVPair(0, root)])
        while (!stack.isEmpty()) {
            let entry = stack.pop().getOrThrow()
            let isVisted = entry.first
            let node = entry.second
            if (let Some(nNode) <- node) {
                if (isVisted == 0) {
                    // ///前序遍历
                    // statck.append((0, node?.right))
                    // statck.append((0, node?.left))
                    // statck.append((1, node))
                    // ///中序遍历
                    // statck.append((0, node?.right))
                    // statck.append((1, node))
                    // statck.append((0, node?.left))

                    ///后序遍历
                    stack.push(KVPair(1, node))
                    stack.push(KVPair(0, nNode.right))
                    stack.push(KVPair(0, nNode.left))
                } else {
                    traversals.append(nNode.val)
                }
            }
        }
        return traversals.toArray()
    }

    /**
    给你二叉搜索树的根节点 root ，同时给定最小边界low 和最大边界 high。通过修剪二叉搜索树，使得所有节点的值在[low, high]中。修剪树 不应该 改变保留在树中的元素的相对结构 (即，如果没有被移除，原有的父代子代关系都应当保留)。 可以证明，存在 唯一的答案 。

    所以结果应当返回修剪好的二叉搜索树的新的根节点。注意，根节点可能会根据给定的边界发生改变
     */
    // MARK: BST  二叉搜索（查找）树
    // MARK: 修剪二叉查找树
    // https://leetcode-cn.com/problems/trim-a-binary-search-tree/description/
    func trimBST(root: ?TreeNode, low: Int, high: Int): ?TreeNode {
        // 通过闭包捕获外面的low和hight
        func trimBSTInner(rt: ?TreeNode): ?TreeNode {
            if (rt.isNone()) {
                return rt
            }
            let rtNode = rt.getOrThrow()
            // 利用搜索树的特点，中间节点大于所有左子树节点，小于所有右子树节点
            // 如果根节点大于hight，说明符合条件的在左子树
            if (rtNode.val > high) {
                return trimBSTInner(rtNode.left)
            }
            // 如果根节点小于low，说明符合条件的在右子树
            if (rtNode.val < low) {
                return trimBSTInner(rtNode.right)
            }
            // 否则剪切左右子树
            rtNode.left = trimBSTInner(rtNode.left)
            rtNode.right = trimBSTInner(rtNode.right)
            return rt
        }
        return trimBSTInner(root)
    }

    /**
    给定一个二叉搜索树的根节点 root ，和一个整数 k ，请你设计一个算法查找其中第 k 小的元素（从 1 开始计数）
     */
    // MARK: 寻找二叉查找树的第 k 个元素
    // https://leetcode-cn.com/problems/kth-smallest-element-in-a-bst/description/
    // 中序遍历解法
    // MARK: 二叉搜索树的第K个结点的值
    // 思想：利用二叉查找树中序遍历有序的特点
    public func kthSmallest(root: ?TreeNode, k: Int): Int {
        var cnt = 0
        var val = 0
        // 根据搜索树的特点，利用中序遍历，遍历到第k个点就是需要的值
        func inOrder(rt: ?TreeNode): Unit {
            if (rt.isNone()) {
                return
            }
            let rtNode = rt.getOrThrow()
            inOrder(rtNode.left)
            cnt += 1
            if (cnt == k) {
                val = rtNode.val
                // 退出内层函数
                return
            }
            inOrder(rtNode.right)
        }
        inOrder(root)
        return val
    }

    // 递归解法
    func kthSmallest2(root: ?TreeNode, k: Int): Int {
        if (root.isNone()) {
            return 0
        }
        // 计算一颗数的所有节点数量
        func count(node: ?TreeNode): Int {
            if (let Some(nNode) <- node) {
                return 1 + count(nNode.left) + count(nNode.right)
            } else {
                return 0
            }
        }
        let rootNode = root.getOrThrow()
        // 计算左子树的数量
        let leftCnt = count(rootNode.left)
        // 如果左子树数量和k相等，说明就是root这个值
        if (leftCnt == k - 1) {
            return rootNode.val
        } else if (leftCnt > k - 1) {
            // 如果左子树的节点数量大于k，说明在左子树中，递归处理左子树
            return kthSmallest(rootNode.left, k)
        } else {
            // 否则，处理剩余的k - leftCnt - 1 个节点
            return kthSmallest(rootNode.right, k - leftCnt - 1)
        }
    }

    /**
    给出二叉 搜索 树的根节点，该树的节点值各不相同，请你将其转换为累加树（Greater Sum Tree），使每个节点 node 的新值等于原树中大于或等于 node.val 的值之和。

    提醒一下，二叉搜索树满足下列约束条件：

    节点的左子树仅包含键 小于 节点键的节点。
    节点的右子树仅包含键 大于 节点键的节点。
    左右子树也必须是二叉搜索树
     */
    // MARK: 把二叉查找树每个节点的值都加上比它大的节点的值
    // https://leetcode-cn.com/problems/convert-bst-to-greater-tree/description/
    func convertBST(root: ?TreeNode): ?TreeNode {
        var sum = 0
        // 利用搜索树的特点，进行后续遍历，sum代表后续遍历的所有节点的和
        func traver(rt: ?TreeNode): Unit {
            if (let Some(rtNode) <- rt) {
                traver(rtNode.right)
                sum += rtNode.val
                rtNode.val = sum
                traver(rtNode.left)
            }
        }
        traver(root)
        return root
    }

    /**
    给定一个二叉搜索树, 找到该树中两个指定节点的最近公共祖先。

    百度百科中最近公共祖先的定义为：“对于有根树 T 的两个结点 p、q，最近公共祖先表示为一个结点 x，满足 x 是 p、q 的祖先且 x 的深度尽可能大（一个节点也可以是它自己的祖先）。”
     */
    // MARK: 二叉查找树的最近公共祖先
    // https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/description/
    public func lowestCommonAncestor(
        root: ?TreeNode,
        p: ?TreeNode,
        q: ?TreeNode
    ): ?TreeNode {
        if (root.isNone()) {
            return root
        }
        let rootNode = root.getOrThrow()
        let rootVal = rootNode.val
        if (rootVal > p.getOrThrow().val && rootVal > q.getOrThrow().val) {
            return lowestCommonAncestor(
                rootNode.left,
                p,
                q
            )
        }
        if (rootVal < p.getOrThrow().val && rootVal < q.getOrThrow().val) {
            return lowestCommonAncestor(
                rootNode.right,
                p,
                q
            )
        }
        return root
    }

    /**
    给定一个二叉树, 找到该树中两个指定节点的最近公共祖先。

    百度百科中最近公共祖先的定义为：“对于有根树 T 的两个节点 p、q，最近公共祖先表示为一个节点 x，满足 x 是 p、q 的祖先且 x 的深度尽可能大（一个节点也可以是它自己的祖先）。
     */
    // MARK: 二叉树的最近公共祖先
    // https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-tree/description/
    public func lowestCommonAncestor1(
        root: ?TreeNode,
        p: ?TreeNode,
        q: ?TreeNode
    ): ?TreeNode {
        /*
             终止条件：
             当越过叶节点，则直接返回 null；
             当 root 等于 p, q ，则直接返回 root ；
         */
        if (root.isNone()) {
            return root
        }
        let rootNode = root.getOrThrow()
        if (rootNode.val == p.getOrThrow().val || rootNode.val == q.getOrThrow().val) {
            return rootNode
        }

        let left = lowestCommonAncestor1(
            rootNode.left,
            p,
            q
        )
        let right = lowestCommonAncestor1(
            rootNode.right,
            p,
            q
        )
        if (left.isNone()) {
            return right
        }
        if (right.isNone()) {
            return left
        }
        return root
    }

    /**
    给你一个整数数组 nums ，其中元素已经按 升序 排列，请你将其转换为一棵 平衡 二叉搜索树
     */
    // MARK: 从有序数组中构造二叉查找树
    // https://leetcode-cn.com/problems/convert-sorted-array-to-binary-search-tree/description/
    func sortedArrayToBST(nums: Array<Int>): ?TreeNode {
        func toBST(sIdx: Int, eIdx: Int): ?TreeNode {
            if (sIdx > eIdx) {
                return None
            }
            let mIdx = sIdx + (eIdx - sIdx) / 2
            let root = TreeNode(nums[mIdx])
            root.left = toBST(sIdx, mIdx - 1)
            root.right = toBST(mIdx + 1, eIdx)
            return root
        }
        return toBST(0, nums.size - 1)
    }

    /**
    给定一个单链表的头节点  head ，其中的元素 按升序排序 ，将其转换为 平衡 二叉搜索树
     */
    // MARK: 根据有序链表构造平衡的二叉查找树
    // https://leetcode-cn.com/problems/convert-sorted-list-to-binary-search-tree/description/
    func sortedListToBST(head: ?ListNode): ?TreeNode {
        if (head.isNone()) {
            return None
        }
        let headNode = head.getOrThrow()
        if (headNode.next.isNone()) {
            return TreeNode(headNode.val)
        }

        // 取链表的中点
        var slow = headNode
        var fast = headNode.next
        var preMid = slow
        while (fast.isSome() && fast.getOrThrow().next.isSome()) {
            preMid = slow
            slow = slow.next.getOrThrow()
            fast = fast.getOrThrow().next.getOrThrow().next
        }

        let mid = preMid.next.getOrThrow()
        preMid.next = None

        let t = TreeNode(mid.val)
        t.left = sortedListToBST(head)
        t.right = sortedListToBST(mid.next)
        return t
    }

    func sortedListToBST2(head: ?ListNode): ?TreeNode {
        let nums = ArrayList<Int>()
        var node = head
        // 将链表转成有序数组nums
        while (let Some(nNode) <- node) {
            nums.append(nNode.val)
            node = nNode.next
        }

        func toBST(sIdx: Int, eIdx: Int): ?TreeNode {
            if (sIdx > eIdx) {
                return None
            }
            let mIdx = sIdx + (eIdx - sIdx) / 2
            let root = TreeNode(nums[mIdx])
            root.left = toBST(sIdx, mIdx - 1)
            root.right = toBST(mIdx + 1, eIdx)
            return root
        }

        return toBST(0, nums.size - 1)
    }

    /** 
    给定一个二叉搜索树 root 和一个目标结果 k，如果二叉搜索树中存在两个元素且它们的和等于给定的目标结果，则返回 true。
     */
    // MARK: 在二叉查找树中寻找两个节点，使它们的和为一个给定值
    // https://leetcode-cn.com/problems/two-sum-iv-input-is-a-bst/description/
    func findTarget(root: ?TreeNode, k: Int): Bool {
        let nums = ArrayList<Int>()
        // 将树转成有序数组nums
        func inOrder(rt: ?TreeNode): Unit {
            if (let Some(rtNode) <- rt) {
                inOrder(rtNode.left)
                nums.append(rtNode.val)
                inOrder(rtNode.right)
            }
        }
        inOrder(root)

        var i = 0
        var j = nums.size - 1
        while (i < j) {
            let sum = nums[i] + nums[j]
            if (sum == k) {
                return true
            } else if (sum < k) {
                i += 1
            } else {
                j -= 1
            }
        }
        return false
    }

    /**
    给你一个二叉搜索树的根节点 root ，返回 树中任意两不同节点值之间的最小差值 。

    差值是一个正数，其数值等于两值之差的绝对值
     */
    // MARK: 在二叉查找树中查找两个节点之差的最小绝对值
    // https://leetcode-cn.com/problems/minimum-absolute-difference-in-bst/description/
    func getMinimumDifference(root: ?TreeNode): Int {
        var minDiff = Int.Max
        var preNode: ?TreeNode = None
        func inOrder(rt: ?TreeNode): Unit {
            if (let Some(rtNode) <- rt) {
                inOrder(rtNode.left)
                if (let Some(pNode) <- preNode) {
                    // 中节点肯定比前一个节点大
                    minDiff = min(minDiff, rtNode.val - pNode.val)
                }
                preNode = rtNode
                inOrder(rtNode.right)
            }
        }
        inOrder(root)
        return minDiff
    }

    /**
    给你一个含重复值的二叉搜索树（BST）的根节点 root ，找出并返回 BST 中的所有 众数（即，出现频率最高的元素）。

    如果树中有不止一个众数，可以按 任意顺序 返回。

    假定 BST 满足如下定义：

    结点左子树中所含节点的值 小于等于 当前节点的值
    结点右子树中所含节点的值 大于等于 当前节点的值
    左子树和右子树都是二叉搜索树
     */
    // MARK: 寻找二叉查找树中出现次数最多的值
    // https://leetcode-cn.com/problems/find-mode-in-binary-search-tree/description/
    func findMode(root: ?TreeNode): Array<Int> {
        let maxCntNums = ArrayList<Int>()
        var preNode: ?TreeNode = None
        var curCnt = 1
        var maxCnt = 1
        func inOrder(rt: ?TreeNode): Unit {
            if (let Some(rtNode) <- rt) {
                inOrder(rtNode.left)
                if (let Some(pNode) <- preNode) {
                    if (pNode.val == rtNode.val) {
                        curCnt += 1
                    } else {
                        curCnt = 1
                    }
                }
                // 如果最大次数有变动，需要将保留的数组清空，并添加最新节点
                if (curCnt > maxCnt) {
                    maxCnt = curCnt
                    maxCntNums.clear()
                    maxCntNums.append(rtNode.val)
                } else if (curCnt == maxCnt) {
                    // 添加同样是最大次数的节点
                    maxCntNums.append(rtNode.val)
                }

                preNode = rtNode
                inOrder(rtNode.right)
            }
        }
        inOrder(root)

        return maxCntNums.toArray()
    }

    public func verify() {
        @dprintMultline(
            // 3
            maxDepth(TreeNode([3, 9, 20, null, null, 15, 7]))
            // true 
            isBalanced(TreeNode([3, 9, 20, null, null, 15, 7]))
            // 3
            diameterOfBinaryTree(TreeNode([1, 2, 3, 4, 5]))
            // [4,7,2,9,6,3,1]
            invertTree(TreeNode([4, 2, 7, 1, 3, 6, 9]))
            // [3,4,5,5,4,null,7]
            mergeTrees(TreeNode([1, 3, 2, 5]), TreeNode([2, 1, 3, null, 4, null, 7]))
            // true
            hasPathSum(TreeNode([5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1]), 22)
            // 3
            pathSum(TreeNode([5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1]), 22)
            // true
            isSubtree(TreeNode([3, 4, 5, 1, 2]), TreeNode([4, 1, 2]))
            // true
            isSymmetric(TreeNode([1, 2, 2, 3, 4, 4, 3]))
            // 5
            minDepth(TreeNode([2, null, 3, null, 4, null, 5, null, 6]))
            // 24
            sumOfLeftLeaves(TreeNode([3, 9, 20, null, null, 15, 7]))
            // 2
            longestUnivaluePath(TreeNode([5, 4, 5, 1, 1, 5]))
            // 7
            rob(TreeNode([3, 2, 3, null, 3, null, 1]))
            rob2(TreeNode([3, 2, 3, null, 3, null, 1]))
            // 5
            findSecondMinimumValue(TreeNode([2, 2, 5, null, null, 5, 7]))
            // [3.00000,14.50000,11.00000]
            averageOfLevels(TreeNode([3, 9, 20, null, null, 15, 7]))
            // 7
            findBottomLeftValue(TreeNode([1, 2, 3, 4, null, 5, 6, null, null, 7]))
            // dfs(TreeNode([1,2,3])
            // visibTreeByColor
            // [1,2,4,5,6,7,3,8,9]
            preorderTraversal(TreeNode([1, 2, 3, 4, 5, null, 8, null, null, 6, 7, 9]))
            // [4,6,7,5,2,9,8,3,1]
            postorderTraversal(TreeNode([1, 2, 3, 4, 5, null, 8, null, null, 6, 7, 9]))
            // [1,3,2]
            inorderTraversal(TreeNode([1, null, 2, 3]))
            // [3,2,null,1]
            trimBST(TreeNode([3, 0, 4, null, 2, null, null, 1]), 1, 3)
            // 3
            kthSmallest(TreeNode([5, 3, 6, 2, 4, null, null, 1]), 3)
            kthSmallest2(TreeNode([5, 3, 6, 2, 4, null, null, 1]), 3)
            // [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
            convertBST(TreeNode([4, 1, 6, 0, 2, 5, 7, null, null, null, 3, null, null, null, 8]))
            // 6 
            lowestCommonAncestor(TreeNode([6, 2, 8, 0, 4, 7, 9, null, null, 3, 5]), TreeNode(2), TreeNode(8))?.val
            // 5
            lowestCommonAncestor1(TreeNode([3, 5, 1, 6, 2, 0, 8, null, null, 7, 4]), TreeNode(5), TreeNode(4))?.val
            // [0,-3,9,-10,null,5] 或者 [0,-10,5,null,-3,null,9]
            sortedArrayToBST([-10, -3, 0, 5, 9])
            // [0,-3,9,-10,null,5] 或者 [0,-10,5,null,-3,null,9]
            sortedListToBST(ListNode([-10, -3, 0, 5, 9]))
            sortedListToBST2(ListNode([-10, -3, 0, 5, 9]))
            // true
            findTarget(TreeNode([5, 3, 6, 2, 4, null, 7]), 9)
            // 1
            getMinimumDifference(TreeNode([1, 0, 48, null, null, 12, 49]))
            // 2
            findMode(TreeNode([1, null, 2, 2]))
        )
    }
}
// cjlint-ignore -end
