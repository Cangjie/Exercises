
## 第2题，两数相加
 * 题目描述：https://leetcode.cn/problems/add-two-numbers/description/
## 解题思路：
    迭代两个List,只要满足 l1不为None、l2不为None、进位不为0 中条件之一，就一直迭代
    每次迭代，sum = l1的值（如果不为空） + l2的值（如果不为空）+ 进位

## 时间、空间复杂度：
    O(n) O(1)

## 仓颉语法：
    Option类型的isSome()和isNone()语法
    Option类型的??语法：a??b, 等同于：if(a.isNone()){b} else {T}
